//Import the Election smart contract
const Election = artifacts.require('Election')
 
//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('Election', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the Election object for testing
    beforeEach(async () => {
        this.Election = await Election.deployed()
    })
 
    //Testing the deployed election contract
    it('deploys successfully', async () => {
        //Get the address which the candidate object is stored
        const address = await this.Election.address
        //Test for valid address
        isValidAddress(address)
    })

    //Testing the content in the contract
  it('test adding candidates', async () => {
    return Election.deployed().then((instance) => {
      ElectionInstance = instance;
      candidate_id = 1;
      return ElectionInstance.add_candidate("0x697b58269742fBA59E11F512f71fE922ef9B4368","Tashi Dema");
    }).then((transaction) => {
      isValidAddress(transaction.tx)
      isValidAddress(transaction.receipt.blockHash);
      return ElectionInstance.candidates_count()
    }).then((count) => {
      assert.equal(count, 2)
      return ElectionInstance.Candidates(1);
    }).then((election) => {
        assert.equal(election.id, candidate_id)
    })
  })

  it('test finding candidate', async () => {
    return Election.deployed().then(async (instance) => {
        s = instance;
        // candidateId = 2;
        return s.add_candidate("0xd036d9F569ac69373cA7fcaD9DC287A4777DEaf9", "Chuki Lhamo").then(async (tx) => {
        return s.add_candidate("0xB20cF0a6eA1379a15Df329Af09fa2A1775443E4C", "Pema Yangzom").then(async (tx) => {
        return s.add_candidate("0xD0d1C130A2F3a4567f326652eD2651706D4b3880", "Dorji Wangmo").then(async (tx) => {
        return s.add_candidate("0xc9ACD40A8FF13fDb5a2d1e6aE5ce2565F55D33Cf", "Kuenza Lhaden").then(async (tx) => {
        return s.add_candidate("0xdA19066B99C213fdA14D7a1C9e2b87EF0B95AE6B", "Chencho Dema").then(async (tx) => {
        return s.add_candidate("0x4A05fea8a3E9ca3ea07BaDc7A5f19B2EbE05A85B", "Tshering Dorji").then(async (tx) => {
        return s.candidates_count().then(async (count) => {
        assert.equal(count, 8)
 
        return s.findCandidate(5).then(async (candidate) => {
            assert.equal(candidate.name, "Kuenza Lhaden")
          })
        })
        })
        })
        })
        })
        })
        })
    })
  })

  it('test voting candidate', async () => {
    return Election.deployed().then(async (instance) => {
        s = instance;
        return s.findCandidate(1).then(async (ocandidate) => {
        assert.equal(ocandidate.name, "Tashi Dema")
        assert.equal(ocandidate.vote_count, 0)
        return s.add_vote(1).then(async (transaction) => {
            return s.findCandidate(1).then(async (ncandidate) => {
            assert.equal(ncandidate.name, "Tashi Dema")
            assert.equal(ncandidate.vote_count, 1)
            return
            })
        })
        })
    })
})



})
//This function check if the address is valid
function isValidAddress(address){
  assert.notEqual(address, 0x0)
  assert.notEqual(address, '')
  assert.notEqual(address, null)
  assert.notEqual(address, undefined)
}