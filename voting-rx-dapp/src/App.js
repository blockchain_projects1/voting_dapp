// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "HTTP://127.0.0.1:7545")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      voteCount: 0,
      candidates: []
    }
  }
  //Display the first account
  render() {
    return (
      <div className="container">
      <h1>Hello, World!</h1>
      <p>Your account: {this.state.account}</p>
    </div>
    );
  }
}
export default App;