// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Election{
    uint public candidates_count;

    struct Candidate{
        string name;
        uint vote_count;
        uint id;
    }

    struct Lead{
        string  name;
        uint votes;
    }

    Lead public the_one;
    
    // Variable that maintains owner address
    address public contractOwner;

    mapping(uint => Candidate)public Candidates;
    mapping(address => bool) public voters;
    mapping(address => bool) public candidate_exist;

    event Registering_candidate(uint candidate_id,  string name, uint total_vote);
    event Voted(uint id);
    event Leader(uint highest_votes, string name);

    // Sets the original owner of contract when it is deployed
    //Constructor for candidate
    constructor(){
        contractOwner = msg.sender;
        add_candidate(0xBB9c4a9Bb65D0863C823deed9bd12f9d8787462B,"Choki Wangmo");
    }
   
    // onlyOwner modifier that validates only if caller of function is contract owner, otherwise not
    modifier onlyOwner(){
    require(contractOwner == msg.sender, "You are not authorised to proceed");
    _;
        
    }
    
    modifier already_voted(){
        require(voters[msg.sender] ==  false, 'The vote has already been casted');
        _;
    }
    
    modifier candidate_registered(address _add){
        require(candidate_exist[_add] == false, "The candidate is already Registered");
        _;
    }

    //store Candidate    
    //fetch Candidate
    //store candidate count
    /* 
        Add new candidates for participating in elections
        The onlyOwner modifie
        
        
        r used here prevents 
        this function to be called by people other 
        than owner who deployed it
    */
    function add_candidate(address _add, string memory _name) public onlyOwner candidate_registered(_add) {
        Candidates[candidates_count] = Candidate(_name, 0, candidates_count);
        candidates_count++;
        candidate_exist[_add] = true;
        emit Registering_candidate(candidates_count, _name, Candidates[candidates_count - 1].vote_count );
    }

    //Fetch student info from storage
    function findCandidate(uint _id) public view returns (Candidate memory) {
        return Candidates[_id];
    }


     // FUNCTION TO vote
    function add_vote(uint _id) public already_voted(){
        require(_id >= 0 && _id < candidates_count, "Invalid option");
        voters[msg.sender] = true;
        Candidates[_id].vote_count++;
        emit Voted(_id);
        // ,Candidates[_id].vote_count
    }

     // FUNCTION TO FIND THE lead
    function leader() public{
        uint max_votes = 0;
        string memory name = "";
        for(uint i=0; i< candidates_count; i++){
            if(max_votes < Candidates[i].vote_count){
                max_votes = Candidates[i].vote_count;
                name = Candidates[i].name;
            }
        }
        the_one = Lead(name, max_votes);
        emit Leader(max_votes, name);
    }

}